# MVP Goal

- [ ] TCP Server
    - [ ] recieve message from any clients
    - [ ] to broadcast messages back to all clients
    - [ ] verify the chat room with a key
    - [ ] Expose the public IP for any telnet or tcp connection


- [ ] Platform Agnostic Client
   - [ ] Web
   - [ ] TUI
   - [ ] GUI

