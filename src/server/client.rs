// We'll create a few common utils and functions which will be used in all clients such as
// connecting to the port, listening to server, etc

#![allow(unused)]
use std::{
    io,
    net::{TcpStream, ToSocketAddrs},
    thread::JoinHandle,
};

#[derive(Debug)]
pub struct Clients {
    pub stream_list: Vec<TcpStream>,
}

fn connect_to_server(addr: String) -> io::Result<TcpStream> {
    TcpStream::connect(addr)
}
