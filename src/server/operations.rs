use crate::utils::{write_log, ChatLog, Message};
use std::{
    io::{Read, Write},
    net::TcpStream,
    sync::mpsc::channel,
    thread,
    time::Instant,
};

pub fn handle_client(stream: TcpStream) -> TcpStream {
    let stream_clone = stream.try_clone().unwrap();
    thread::spawn(move || {
        connect_client(stream);
    });
    stream_clone
}

pub fn connect_client(stream: TcpStream) {
    let mut _clients: Vec<TcpStream> = Vec::new();
    let mut _chatlog = ChatLog::new();
    let (send_msg_chan_1, rec_msg_chan_1) = channel::<Message>();

    println!("Connected, {}", stream.peer_addr().unwrap());

    let stream_clone = stream.try_clone().unwrap();
    thread::spawn(move || loop {
        let message = read_from_client(stream_clone.try_clone().unwrap());
        send_msg_chan_1
            .send(message.0.clone())
            .map_err(|err| eprintln!("ERROR: unable to send message across thread,{err}"))
            .unwrap();
    });

    thread::spawn(move || loop {
        write_log(
            rec_msg_chan_1
                .recv()
                .map_err(|err| eprintln!("ERROR: recieving message from thread,{err}"))
                .unwrap(),
        )
        .unwrap();
    });
}

pub fn write_into_client(stream: &mut TcpStream, message: Message) {
    let msg = format!("{:?}", message.msg);
    if let Err(err) = stream.write_all(msg.as_bytes()) {
        eprintln!("ERROR: writing to stream, {err}")
    };
}

pub fn read_from_client(mut stream: TcpStream) -> (Message, TcpStream) {
    let mut message_buffer = [0_u8; 200];
    let mut msg_string = String::new();

    stream
        .read(&mut message_buffer)
        .map_err(|err| {
            eprintln!("ERROR: reading from stream, {err}");
        })
        .unwrap();
    for msg in message_buffer {
        if msg == 0 {
            break;
        }
        msg_string.push(msg as char);
    }
    (
        Message {
            addr: stream.local_addr().unwrap(),
            msg: msg_string,
            time: Instant::now(),
        },
        stream.try_clone().unwrap(),
    )
}
