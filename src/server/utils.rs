use std::{
    fs::OpenOptions,
    io::{self, Write},
    net::{SocketAddr, SocketAddrV4, TcpStream},
    str::FromStr,
    time::Instant,
};

use chrono::{self, Utc};

pub const DEFAULT_PORT: &str = "8000";

pub struct ChatLog {
    pub messages: Vec<Message>,
}

impl Default for ChatLog {
    fn default() -> Self {
        Self::new()
    }
}

impl ChatLog {
    pub fn new() -> Self {
        Self {
            messages: Vec::new(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Message {
    pub addr: SocketAddr,
    pub msg: String,
    pub time: Instant,
}

impl Message {
    pub fn my_default(stream: TcpStream) -> Self {
        Self {
            addr: stream.local_addr().unwrap(),
            msg: stream.peer_addr().unwrap().to_string(),
            time: Instant::now(),
        }
    }
}

impl Default for Message {
    fn default() -> Self {
        Message {
            addr: SocketAddr::from(SocketAddrV4::from_str("127.0.0.1:8000").unwrap()),
            msg: String::new(),
            time: Instant::now(),
        }
    }
}

pub fn write_log(message: Message) -> io::Result<()> {
    let msg_str = format!("{:?} : {:?}", Utc::now(), message);

    let mut file = OpenOptions::new()
        .create(true)
        .append(true)
        .open("chat.log")
        .map_err(|err| eprintln!("ERROR: unable to open file, {err}"))
        .unwrap();

    if let Err(err) = writeln!(file, "{}", msg_str) {
        eprintln!("ERROR: writing to file, {err}")
    };
    Ok(())
}
