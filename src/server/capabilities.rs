#[derive(Debug)]
pub enum ServerCapabilites {
    Read,
    Write,
    BroadCast,
}
