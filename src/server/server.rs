use crate::{
    client::Clients,
    operations::{handle_client, write_into_client},
    utils::{Message, DEFAULT_PORT},
};
use std::{
    env,
    net::{TcpListener, TcpStream},
    sync::{mpsc::channel, Arc, Mutex},
    thread,
};

#[derive(Debug)]
pub struct Server {
    pub listener: TcpListener,
    pub chats: Vec<Message>,
    pub name: String,
}

impl Server {
    pub fn start(name: String) -> Self {
        let port: Result<String, env::VarError> = env::var("SERVER_PORT");
        let addr: String = "127.0.0.1:".to_owned() + &port.unwrap_or(DEFAULT_PORT.into());

        Self {
            listener: TcpListener::bind(addr)
                .map_err(|err| eprintln!("ERROR: binding to port, {err}"))
                .unwrap(),
            chats: Vec::new(),
            name,
        }
    }
    pub fn run_server(&mut self) {
        let clients = Arc::new(Mutex::new(Clients {
            stream_list: Vec::new(),
        }));
        let (sendx, recv) = channel::<TcpStream>();
        let client_clone = Arc::clone(&clients);
        thread::spawn(move || loop {
            let rec_client = recv.recv().unwrap();
            client_clone
                .clone()
                .lock()
                .unwrap()
                .stream_list
                .push(rec_client);
            for stream in client_clone.lock().unwrap().stream_list.iter_mut() {
                let stream_msg = stream.try_clone().unwrap();
                write_into_client(stream, Message::my_default(stream_msg));
            }
        });
        for stream in self.listener.incoming() {
            match stream {
                Ok(stream) => {
                    let sendx = sendx.clone();
                    thread::spawn(move || {
                        let sent_client_chan = handle_client(stream);
                        sendx.clone().send(sent_client_chan).unwrap();
                    });
                }
                Err(err) => {
                    eprintln!("ERROR: listening to a client, {err}")
                }
            }
        }
    }
}
