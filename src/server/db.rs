use std::io::stdin;

use rusqlite::{Connection, Result};

pub fn connect_db() -> (Result<Connection>, String, String) {
    let stdin = stdin();
    let mut server_name = String::new();
    let mut table_name = String::new();

    println!("Enter the name of the server");
    stdin
        .read_line(&mut server_name)
        .map_err(|err| eprintln!("ERROR: accepting server name, {err}"))
        .unwrap();
    println!("Enter the name of the database table");
    stdin
        .read_line(&mut table_name)
        .map_err(|err| eprintln!("ERROR: accepting server name, {err}"))
        .unwrap();

    (Connection::open("./chat.db"), server_name, table_name)
}
