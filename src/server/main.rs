mod capabilities;
mod client;
mod db;
mod operations;
mod server;
mod tests;
mod utils;

// Std lib
use std::env;

// Project crate
use server::Server;

fn main() -> rusqlite::Result<()> {
    let mut server = Server::start("HELLO".into());
    println!(
        "Listenting on port:127.0.0.1: {}",
        env::var("SERVER_PORT").unwrap()
    );

    server.run_server();
    Ok(())
}
